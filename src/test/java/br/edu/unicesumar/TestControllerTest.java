package br.edu.unicesumar;

import java.nio.charset.Charset;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@Transactional
public class TestControllerTest {

	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mvc = MockMvcBuilders.webAppContextSetup(context).dispatchOptions(true).build();
	}

	private ResultActions get(final String url) throws Exception {
		return mvc.perform(MockMvcRequestBuilders.get(url));
	}

	private ResultActions post(final String url, final String postMessage) throws Exception {
		final MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post(url);
		builder.content(postMessage);
		builder.contentType(new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(),
				Charset.forName("utf8")));

		return mvc.perform(builder);
	}

	@Test
	@Rollback(true)
	public void getTests() throws Exception {
		get("/test/all")
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().string(Matchers.equalTo("[]")));
	}

	@Test
	@Rollback(true)
	public void postTest() throws Exception {
		post("/test", "{\"name\":\"ESCOLA DE TI 2016\"}")
				.andExpect(MockMvcResultMatchers.status().isAccepted())
				.andExpect(MockMvcResultMatchers.content().string(
						Matchers.equalTo("{\"id\":5,\"name\":\"ESCOLA DE TI 2016\"}")));
		
		get("/test/all")
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().string(
						Matchers.equalTo("[{\"id\":5,\"name\":\"ESCOLA DE TI 2016\"}]")));
	}

	@Test
	@Rollback(true)
	public void postTestMultiple() throws Exception {
		post("/test", "{\"name\":\"ESCOLA DE TI 2016\"}")
				.andExpect(MockMvcResultMatchers.status().isAccepted())
				.andExpect(MockMvcResultMatchers.content().string(
						Matchers.equalTo("{\"id\":1,\"name\":\"ESCOLA DE TI 2016\"}")));

		post("/test", "{\"name\":\"ENGENHARIA DE SOFTWARE\"}")
				.andExpect(MockMvcResultMatchers.status().isAccepted())
				.andExpect(MockMvcResultMatchers.content().string(
						Matchers.equalTo("{\"id\":2,\"name\":\"ENGENHARIA DE SOFTWARE\"}")));

		post("/test", "{\"name\":\"ANALISE E DESENVOLVIMENTO DE SISTEMAS\"}")
				.andExpect(MockMvcResultMatchers.status().isAccepted())
				.andExpect(MockMvcResultMatchers.content().string(
						Matchers.equalTo("{\"id\":3,\"name\":\"ANALISE E DESENVOLVIMENTO DE SISTEMAS\"}")));

		post("/test", "{\"name\":\"SISTEMAS PARA INTERNET\"}")
				.andExpect(MockMvcResultMatchers.status().isAccepted())
				.andExpect(MockMvcResultMatchers.content().string(
						Matchers.equalTo("{\"id\":4,\"name\":\"SISTEMAS PARA INTERNET\"}")));
		
		get("/test/all")
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().string(
						Matchers.equalTo(
								"["
								+ "{\"id\":1,\"name\":\"ESCOLA DE TI 2016\"},"
								+ "{\"id\":2,\"name\":\"ENGENHARIA DE SOFTWARE\"},"
								+ "{\"id\":3,\"name\":\"ANALISE E DESENVOLVIMENTO DE SISTEMAS\"},"
								+ "{\"id\":4,\"name\":\"SISTEMAS PARA INTERNET\"}"
								+ "]")));
	}
}
