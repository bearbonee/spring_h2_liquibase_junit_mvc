package br.edu.unicesumar.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.edu.unicesumar.model.Test;
import br.edu.unicesumar.repository.TestRepository;

@RestController
@RequestMapping("/test")
public class TestController {

	@Autowired
	private TestRepository repository;

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ResponseEntity<List<Test>> getAll() {
		return new ResponseEntity<>(repository.findAll(), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Test> create(@RequestBody final Test test) {
		return new ResponseEntity<>(repository.save(test), HttpStatus.ACCEPTED);
	}
}
